import 'package:afsar_demo/Dashbord.dart';
import 'package:afsar_demo/RegiterWidget.dart';
import 'package:afsar_demo/SplashWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'Util.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SplashWidget(),
    );
  }
}

TextEditingController mailController = TextEditingController();
TextEditingController passwordController = TextEditingController();

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  Animation<double> _iconAnimation;
  AnimationController _iconAnimationController;

  @override
  void initState() {
    super.initState();
    _iconAnimationController = new AnimationController(
        vsync: this, duration: new Duration(milliseconds: 500));
    _iconAnimation = new CurvedAnimation(
      parent: _iconAnimationController,
      curve: Curves.bounceOut,
    );
    _iconAnimation.addListener(() => this.setState(() {}));
    _iconAnimationController.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Login'),),
      body: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          image: DecorationImage(
              fit: BoxFit.cover,
            image: AssetImage('assets/girl.jpeg',)
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
//            GestureDetector(
//              onTap: () {
//                customDialog(
//                    context,
//                    title: 'Logo Dialog',
//                    widget: Image.asset(
//                      'assets/girl.jpeg',
//                      height: 50,
//                      width: 50,
//                    ),
//                    onClick: (){
//                      print('logo click here');
//                    }
//                );
//              },
//
//
//
//
//            ),
            /*FlutterLogo(
              size: _iconAnimation.value * 140.0,
            ),*/

            Hero(
            tag: 'mylogo',
            child: GestureDetector(
              onTap: () =>  Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => RegiterWidget()),
              ),
              child: Image.asset(
                'assets/logo.jpg',
                width: 150.0,
                height: 150.0,
              ),
            ),
//

            ),
            new Container(

              padding: const EdgeInsets.all(40.0),
              child: new Form(
                autovalidate: true,
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new TextFormField(
                      controller: mailController,
                      decoration: new InputDecoration(
                          labelText: "Enter Email", fillColor: Colors.white),
                      keyboardType: TextInputType.emailAddress,
                    ),
                    new TextFormField(
                      controller: passwordController,
                      decoration: new InputDecoration(
                        labelText: "Enter Password",
                      ),
                      obscureText: true,
                      keyboardType: TextInputType.text,
                    ),
                    new Padding(
                      padding: const EdgeInsets.only(top: 60.0),
                    ),
                    Row(

                      children: <Widget>[
                        new MaterialButton(
                          height: 50.0,
                          minWidth: 120.0,
                          color: Colors.green,
                          splashColor: Colors.teal,
                          textColor: Colors.white,
                          child: new Text("Login"),
                          onPressed: ()  {

                            String mail = mailController.text.toString();
                            String password = passwordController.text.toString();
                            if (mail.isEmpty)
                              toast(message: 'Please Enter Email');
                            else if(password.isEmpty)
                              toast(message: 'Please Enter Password');
                            else
                              toast(message: '$mail   $password');

//                         var image = await ImagePicker.pickImage(source: ImageSource.gallery);
//                         customDialog(context,title: 'Image Picker',widget: Image.file(image,height: 250,width: 200,));
                            /*String mail = mailController.text.toString();
                                String password = passwordController.text.toString();
                                if (mail.isNotEmpty && password.isNotEmpty)
                                  customDialog(context,
                                      title: 'Login Page',
                                      onClick: (){
                                    print('login click');
                                      },
                                      widget: Text('$mail,$password'));*/
                          },
                        ),
                        new Padding(
                          padding: const EdgeInsets.only(right: 20.0),
                        ),
                        new MaterialButton(
                          height: 50.0,
                          minWidth: 120.0,
                          color: Colors.green,
                          splashColor: Colors.teal,
                          textColor: Colors.white,
                          child: new Text("Register Here"),
                          onPressed: ()  {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => RegiterWidget()),
                            );
                          },
                        ),


                      ],
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {

            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Dashboard()),
            );

        },
        child: Icon(Icons.contacts),
        backgroundColor: Colors.green,
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
  data(){
    new Stack(fit: StackFit.expand, children: <Widget>[
      new Image(
        image: new AssetImage("assets/girl.jpeg"),
        fit: BoxFit.cover,
        colorBlendMode: BlendMode.darken,
        color: Colors.black87,
      ),
      new Theme(
        data: new ThemeData(
            brightness: Brightness.dark,
            inputDecorationTheme: new InputDecorationTheme(
              // hintStyle: new TextStyle(color: Colors.blue, fontSize: 20.0),
              labelStyle:
              new TextStyle(color: Colors.tealAccent, fontSize: 20.0),
            )),
        isMaterialAppTheme: true,
        child: SingleChildScrollView(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  customDialog(
                      context,
                      title: 'Logo Dialog',
                      widget: Image.asset(
                        'assets/girl.jpeg',
                        height: 50,
                        width: 50,
                      ),
                      onClick: (){
                        print('logo click here');
                      }
                  );
                },
                child:  new Image.asset(
                  'assets/logo.jpg',
                  width: 150.0,
                  height: 150.0,
                ),

              ),
              new Container(

                padding: const EdgeInsets.all(40.0),
                child: new Form(
                  autovalidate: true,
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new TextFormField(
                        controller: mailController,
                        decoration: new InputDecoration(
                            labelText: "Enter Email", fillColor: Colors.white),
                        keyboardType: TextInputType.emailAddress,
                      ),
                      new TextFormField(
                        controller: passwordController,
                        decoration: new InputDecoration(
                          labelText: "Enter Password",
                        ),
                        obscureText: true,
                        keyboardType: TextInputType.text,
                      ),
                      new Padding(
                        padding: const EdgeInsets.only(top: 60.0),
                      ),
                      Row(

                        children: <Widget>[
                          new MaterialButton(
                            height: 50.0,
                            minWidth: 120.0,
                            color: Colors.green,
                            splashColor: Colors.teal,
                            textColor: Colors.white,
                            child: new Text("Login"),
                            onPressed: ()  {
//                         var image = await ImagePicker.pickImage(source: ImageSource.gallery);
//                         customDialog(context,title: 'Image Picker',widget: Image.file(image,height: 250,width: 200,));
                              /*String mail = mailController.text.toString();
                                String password = passwordController.text.toString();
                                if (mail.isNotEmpty && password.isNotEmpty)
                                  customDialog(context,
                                      title: 'Login Page',
                                      onClick: (){
                                    print('login click');
                                      },
                                      widget: Text('$mail,$password'));*/
                            },
                          ),
                          new Padding(
                            padding: const EdgeInsets.only(right: 20.0),
                          ),
                          new MaterialButton(
                            height: 50.0,
                            minWidth: 120.0,
                            color: Colors.green,
                            splashColor: Colors.teal,
                            textColor: Colors.white,
                            child: new Text("Register Here"),
                            onPressed: ()  {
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => RegiterWidget()),
                              );
                            },
                          ),


                        ],
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    ]);
  }
}
