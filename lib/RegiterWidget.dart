import 'package:flutter/material.dart';

import 'main.dart';

class RegiterWidget extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Register Here"),),
      body: new Stack(fit: StackFit.expand, children: <Widget>[
        new Image(
          image: new AssetImage("assets/girl.jpeg"),
          fit: BoxFit.cover,
          colorBlendMode: BlendMode.darken,
          color: Colors.black87,
        ),
        new Theme(
          data: new ThemeData(
              brightness: Brightness.dark,
              inputDecorationTheme: new InputDecorationTheme(
                // hintStyle: new TextStyle(color: Colors.blue, fontSize: 20.0),
                labelStyle:
                new TextStyle(color: Colors.tealAccent, fontSize: 20.0),
              )),
          isMaterialAppTheme: true,
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
//              GestureDetector(
//                onTap: () {
//
//                },
//                child:  new Image.asset(
//                  'assets/logo.jpg',
//                  width: 150.0,
//                  height: 150.0,
//                ),
//
//              ),

              Hero(
                tag: 'mylogo',
                child: Image.asset(
                  'assets/logo.jpg',
                  width: 150.0,
                  height: 150.0,
                ),

              ),
              new Container(

                padding: const EdgeInsets.all(40.0),
                child: new Form(
                  autovalidate: true,
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[

                      new TextFormField(

                        decoration: new InputDecoration(
                          labelText: "Enter Name",
                        ),
                        keyboardType: TextInputType.text,
                      ),

                      new TextFormField(

                        decoration: new InputDecoration(
                            labelText: "Enter Email", fillColor: Colors.white),
                        keyboardType: TextInputType.emailAddress,
                      ),
                      new TextFormField(

                        decoration: new InputDecoration(
                          labelText: "Enter Password",
                        ),
                        obscureText: true,
                        keyboardType: TextInputType.text,
                      ),

                      new Padding(
                        padding: const EdgeInsets.only(top: 60.0),
                      ),
                      Row(

                        children: <Widget>[
                          new MaterialButton(
                            height: 50.0,
                            minWidth: 120.0,
                            color: Colors.green,
                            splashColor: Colors.teal,
                            textColor: Colors.white,
                            child: new Text("Register Here"),
                            onPressed: ()  {
//                         var image = await ImagePicker.pickImage(source: ImageSource.gallery);
//                         customDialog(context,title: 'Image Picker',widget: Image.file(image,height: 250,width: 200,));
                              /*String mail = mailController.text.toString();
                              String password = passwordController.text.toString();
                              if (mail.isNotEmpty && password.isNotEmpty)
                                customDialog(context,
                                    title: 'Login Page',
                                    onClick: (){
                                  print('login click');
                                    },
                                    widget: Text('$mail,$password'));*/
                            },
                          ),
                          new Padding(
                            padding: const EdgeInsets.only(right: 20.0),
                          ),
                          new MaterialButton(
                            height: 50.0,
                            minWidth: 120.0,
                            color: Colors.green,
                            splashColor: Colors.teal,
                            textColor: Colors.white,
                            child: new Text("Login Here"),
                            onPressed: ()  {
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => MyHomePage()),
                              );
                            },
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ]),
    );
  }
}
