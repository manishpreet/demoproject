import 'package:afsar_demo/ListWidget.dart';
import 'package:afsar_demo/Viewimg.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart'as http;
import 'dart:async';
import 'dart:convert';

import 'Util.dart';


class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  Map data;
  List image;

  Future getData() async {
    http.Response response = await http.get("https://pixabay.com/api/?key=16008463-0462e30ab693e5d8d65243184&q=yellow+flowers&image_type=photo&pretty=true");
    data = jsonDecode(response.body);
    setState(() {
      image = data["hits"];
    });

  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
  }
  //List<String>names = ['manish','afsar','kumar','one','twp','three','four'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Builder(
            builder: (BuildContext context){
              return IconButton(
                icon: Icon(Icons.menu),
                onPressed: () {

                },
              );
            }),
        title: Text("Services"),
        centerTitle: true,

        backgroundColor: Colors.black87,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search,color: Colors.orange),
            onPressed: () {
            },
          ),

        ],
      ),
body: GridView.builder(
  itemCount: image == null ? 0 : image.length,
  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
      crossAxisCount: 2),
  itemBuilder: (BuildContext context, int index) {
    return new
      
    Card(
            elevation: 1.0,
            margin: new EdgeInsets.all(2.0),
            child: InkWell(
              onTap: (){
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => Viewimg(
              )));
    },

              child: Stack(fit: StackFit.expand,
                  children: [
                  FadeInImage.assetNetwork(
              placeholder: 'assets/placeholder.gif',
              image: image[index]['webformatURL'],
                    fit: BoxFit.fill,
            ),
    ],
    )
//              child: Container(
//                decoration: BoxDecoration(
//                image: new DecorationImage(image: new NetworkImage(image[index]['webformatURL']),
//                  fit: BoxFit.cover
//                ),
//
//    ),
//    ),
            ),
    );
  },
),
//      body: Container(
//        color: Colors.black,
//        padding: EdgeInsets.symmetric(vertical: 2.0, horizontal: 1.0),
//        child: GridView.count(
//          crossAxisCount: 2,
//          padding: EdgeInsets.all(2.0),
//          children: <Widget>[
////            makeDashboardItem("Ordbog","Subheading", Icons.book,function: (){
////              navigate(context: context,screen: ListWidget());
////            }),
////            makeDashboardItem("Alphabet","Subheading", Icons.alarm),
////            makeDashboardItem("Alphabet","Subheading", Icons.alarm),
////            makeDashboardItem("Alphabet","Subheading", Icons.alarm),
////            makeDashboardItem("Alphabet","Subheading", Icons.alarm),
////            makeDashboardItem("Alphabet","Subheading", Icons.alarm),
////            makeDashboardItem("Alphabet","Subheading", Icons.alarm),
////            makeDashboardItem("Alphabet","Subheading", Icons.alarm,function: (){
////
////            })
//
//            Card(
//            elevation: 1.0,
//            margin: new EdgeInsets.all(2.0),
//            child: Container(
//              decoration: BoxDecoration(color: Colors.black87),
//              child: new InkWell(
//                onTap: () {
//
//                },
//                child: Column(
//                  crossAxisAlignment: CrossAxisAlignment.stretch,
//                  mainAxisSize: MainAxisSize.min,
//                  verticalDirection: VerticalDirection.down,
//                  children: <Widget>[
//                    SizedBox(height: 50.0),
//                    Center(
//                        child: Icon(
//                          Icons.book,
//                          size: 40.0,
//                          color: Colors.white,
//                        )),
//                    SizedBox(height: 15.0),
//                    new Center(
//                      child: new Text("title",
//                          style: new TextStyle(fontSize: 18.0, color: Colors.white)),
//                    ),
//                    SizedBox(height: 4.0),
//                    new Center(
//                      child: new Text("subtitle",
//                          style: new TextStyle(fontSize: 10.0, color: Colors.orange)),
//                    )
//                  ],
//                ),
//              ),
//            )),
//
//
//
//          ],
//        ),
//      ),
    );
  }
}
