import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

customDialog(BuildContext context,
    {Widget widget, String title, Function onClick}) {
  AlertDialog androidAlert = AlertDialog(
    title: Text(title),
    content: widget,
    actions: [
      FlatButton(
        child: Text("OK"),
        onPressed: () {
          Navigator.of(context).pop();
          onClick();
        },
      ),
    ],
  );
  CupertinoAlertDialog iosAlert = new CupertinoAlertDialog(
    title: new Text(title),
    content: widget,
    actions: <Widget>[
      CupertinoDialogAction(
        isDefaultAction: true,
        onPressed: () {
          Navigator.pop(context);
          onClick();
        },
        child: Text("OK"),
      ),
    ],
  );

  // show the dialog

  if (Platform.isAndroid)
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return androidAlert;
      },
    );
  else
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return iosAlert;
      },
    );
}

toast({String message = ''}) {
  Fluttertoast.showToast(
    msg: message,
    toastLength: Toast.LENGTH_SHORT,
    gravity: ToastGravity.BOTTOM,
  );
}

navigate({BuildContext context, Widget screen, bool isFinish = false}) {
  if (isFinish)
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => screen));
  else
    Navigator.push(context, MaterialPageRoute(builder: (context) => screen));
}

Card makeDashboardItem(String title, String subtitle, IconData icon,
    {Function function}) {
  return Card(
      elevation: 1.0,
      margin: new EdgeInsets.all(2.0),
      child: Container(
        decoration: BoxDecoration(color: Colors.black87),
        child: new InkWell(
          onTap: () {
            function();
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            verticalDirection: VerticalDirection.down,
            children: <Widget>[
              SizedBox(height: 50.0),
              Center(
                  child: Icon(
                icon,
                size: 40.0,
                color: Colors.white,
              )),
              SizedBox(height: 15.0),
              new Center(
                child: new Text(title,
                    style: new TextStyle(fontSize: 18.0, color: Colors.white)),
              ),
              SizedBox(height: 4.0),
              new Center(
                child: new Text(subtitle,
                    style: new TextStyle(fontSize: 10.0, color: Colors.orange)),
              )
            ],
          ),
        ),
      ));
}
