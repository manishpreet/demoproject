import 'package:afsar_demo/Dashbord.dart';
import 'package:afsar_demo/Util.dart';
import 'package:afsar_demo/main.dart';
import 'package:flutter/material.dart';

class SplashWidget extends StatefulWidget {

  @override
  _SplashWidgetState createState() => _SplashWidgetState();
}

class _SplashWidgetState extends State<SplashWidget> {

  @override
  void initState() {
    super.initState();
    timer();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _body(context),
    );
  }

  _body(BuildContext context) {
    return Center(
      child: Image.asset('assets/logo.jpg'),
    );
  }

  Future<void> timer() async {
    await Future.delayed(Duration(seconds: 4));
    navigate(context: context,screen: Dashboard(),isFinish: true);
    //navigate(context: context,screen: MyHomePage());
  }
}
