import 'package:afsar_demo/Util.dart';
import 'package:flutter/material.dart';

class ListWidget extends StatelessWidget {
  List<String>names = ['manish','afsar','kumar','one','twp','three'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('ListView'),),
      body: _body(context),
    );
  }

  _body(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: ListView.builder(
        itemCount: names.length,
        itemBuilder: (con,ind) {
          return listItem(names[ind],con,ind);
        },
      ),
    );
  }

  listItem(String nam, BuildContext context, int position) {
    return InkWell(
      onTap: (){
        toast(message: nam.toString());
      },
      child: Container(
          padding: EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(nam),
              SizedBox(height: 12.0,),
              Container(width: MediaQuery.of(context).size.width,
                height: 0.5,
                color: Colors.black26,)
            ],
          )),
    );
  }

 /* return Container(
  padding: EdgeInsets.all(16.0),
  child: ListView(
  children: names.map((n) => InkWell(
  onTap: (){
  toast(message: names.indexOf(n).toString());
  },
  child: Container(
  padding: EdgeInsets.all(8.0),
  child: Column(
  crossAxisAlignment: CrossAxisAlignment.start,
  children: <Widget>[
  Text(n),
  SizedBox(height: 12.0,),
  Container(width: MediaQuery.of(context).size.width,
  height: 0.5,
  color: Colors.black26,)
  ],
  )),
  )).toList(),
  ),
  );*/
}
